package com.example.gimnasiosapp.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.gimnasiosapp.model.Gym
import com.example.gimnasiosapp.model.Gyms
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File

class GymsViewModel: ViewModel() {
    private val repository = Repository()
    var gym = MutableLiveData<Gym>()
    val gyms = MutableLiveData<List<Gym>>()
    var gymImage = MutableLiveData<File>()

    fun fetchData() {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getGyms()
            withContext(Dispatchers.Main) {
                if(response.isSuccessful && response.body() != null){
                    gyms.postValue(response.body())
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }


}
