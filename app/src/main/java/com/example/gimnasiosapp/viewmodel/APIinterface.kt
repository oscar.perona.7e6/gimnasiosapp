package com.example.gimnasiosapp.viewmodel

import com.example.gimnasiosapp.model.Gym
import com.example.gimnasiosapp.model.Gyms
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import java.io.File

interface ApiInterface {

    @GET("gimnasios")
    suspend fun getGyms(): Response<List<Gym>>

    @GET("gimnasios/uploads/{imageName}")
    suspend fun getGymImg(@Path("imageName") imageName: String): Response<ByteArray>
    companion object {
        val BASE_URL = "http://192.168.1.49:8080/" //La ip es la ip en la que se ejecuta el proyecto
        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }

    /*
        192.168.1.49 : pc Oscar
     */

}