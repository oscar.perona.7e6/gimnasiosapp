package com.example.gimnasiosapp.viewmodel

class Repository {
    val apiInterface = ApiInterface.create()

    suspend fun getGyms() = apiInterface.getGyms()

    suspend fun getGymImg(imageName:String) = apiInterface.getGymImg(imageName)

}
