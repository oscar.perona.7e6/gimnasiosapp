package com.example.gimnasiosapp.viewmodel

import com.example.gimnasiosapp.model.Gym

interface OnClickListener {
    fun onClick(gym: Gym)
}
