package com.example.gimnasiosapp.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.gimnasiosapp.R
import com.example.gimnasiosapp.databinding.FragmentLogInBinding

class LogInFragment : Fragment() {

    lateinit var binding: FragmentLogInBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLogInBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.logInButton.setOnClickListener {
            val username = binding.userText.text.toString()
            val password = binding.passwText.text.toString()

            if (username != "" && password != ""){

                findNavController().navigate(R.id.action_logInFragment_to_allGymFragment)

            }
            else if (username == "" && password == ""){
                Toast.makeText(activity, "Error al iniciar sesion. No has introducido usuario y contraseña", Toast.LENGTH_SHORT).show()
            }
            else if (username == "" && password != ""){
                Toast.makeText(activity, "Error al iniciar sesion. No has introducido contraseña", Toast.LENGTH_SHORT).show()

            }
            else Toast.makeText(activity, "Error al iniciar sesion. No has introducido contraseña", Toast.LENGTH_SHORT).show()

        }
    }

}