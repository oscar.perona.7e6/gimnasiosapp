package com.example.gimnasiosapp.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.gimnasiosapp.databinding.FragmentAllGymBinding
import com.example.gimnasiosapp.model.Gym
import com.example.gimnasiosapp.model.GymAdapter
import com.example.gimnasiosapp.viewmodel.GymsViewModel
import com.example.gimnasiosapp.viewmodel.OnClickListener


class AllGymFragment : Fragment(), OnClickListener {

    private lateinit var gymAdapter: GymAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    lateinit var binding: FragmentAllGymBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAllGymBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        gymAdapter = GymAdapter(getGyms(), this)
        linearLayoutManager = LinearLayoutManager(context)

        val gymViewModel = ViewModelProvider(requireActivity())[GymsViewModel::class.java]

        gymViewModel.fetchData()

        gymViewModel.gyms.observe(viewLifecycleOwner){
            setUpRecyclerView(it)
            Toast.makeText(activity, "${gymViewModel.gyms.value!!.size}", Toast.LENGTH_SHORT).show()
        }
    }

    private fun getGyms(): MutableList<Gym>{
        val gyms = mutableListOf<Gym>()
        gyms.add(Gym(1, "nombre", "ubicacion","contacto","paginaOficial","company","imagen"))
        return gyms
    }

    private fun setUpRecyclerView(gyms: List<Gym>) {
        gymAdapter = GymAdapter(gyms, this)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = gymAdapter
        }


    }

    override fun onClick(gym: Gym) {
            // IR a fragment del detalle
    }
}