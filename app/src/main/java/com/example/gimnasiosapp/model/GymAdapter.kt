package com.example.gimnasiosapp.model

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.gimnasiosapp.R
import com.example.gimnasiosapp.databinding.ItemGymBinding
import com.example.gimnasiosapp.viewmodel.GymsViewModel
import com.example.gimnasiosapp.viewmodel.OnClickListener
import com.example.gimnasiosapp.viewmodel.Repository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class GymAdapter(private val gyms: List<Gym>,private val listener: OnClickListener): RecyclerView.Adapter<GymAdapter.ViewHolder>() {

    val repository = Repository()

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemGymBinding.bind(view)
        fun setListener(gym: Gym){
            binding.root.setOnClickListener {
                listener.onClick(gym)
            }
        }
    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_gym, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return gyms.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val gym = gyms[position]

        with(holder){
            setListener(gym)
            binding.gymName.text = gym.nombre
            binding.gymDirecction.text = gym.ubicacion
            CoroutineScope(Dispatchers.IO).launch {
                val response = repository.getGymImg(gym.imagen)
                withContext(Dispatchers.Main) {
                    if(response.isSuccessful && response.body() != null){

                        val fileName = gym.imagen
                        val image = repository.getGymImg(fileName)

                        Glide.with(context)
                            .load(image)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .centerCrop()
                            .circleCrop()
                            .into(binding.img)

                    }
                    else{
                        Log.e("Error :", response.message())
                    }
                }
            }
        }

        //fetchImage(holder, gym)
    }

    fun fetchImage(holder: ViewHolder, gym:Gym) {


    }

}
