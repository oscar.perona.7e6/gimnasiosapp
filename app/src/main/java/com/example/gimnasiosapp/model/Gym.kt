package com.example.gimnasiosapp.model

data class Gym(
    val id:Int,
    val nombre:String,
    val ubicacion:String,
    val contacto:String,
    val paginaOfical:String,
    val company:String,
    val imagen:String
    )